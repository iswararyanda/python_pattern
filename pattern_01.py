"""
Pattern-01

# # # # #
# # # # #
# # # # #
# # # # #
# # # # #
"""

baris = int(input("Masukan jumlah baris: "))

for i in range(baris):
	for j in range(baris):
		print ("*", end=" ")
	print()

print("=" * 20)

i = 0

while i < baris:
	j = 0
	while j < baris:
		print("*", end=" ")

		j += 1

	print()
	i += 1