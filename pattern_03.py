"""
pattern_03

* * * * *
* * * *
* * *
* *
*
"""

baris = int(input("Masukan jumlah baris: "))

for i in range(baris):
	for j in range(baris-i):
		print("*", end=" ")
	print()

print()

k = 0
while k < baris:
	l = 0
	while l < (baris-k):
		print("*", end=" ")

		l += 1

	print()
	k += 1
		