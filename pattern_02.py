"""
pattern_02

*
* *
* * *
* * * *
* * * * *
"""

baris = int(input("Masukan jumlah baris: "))

for i in range(baris):
	for j in range(i+1):
		print("*", end=" ")

	print()

print()

k = 0
while k < baris:
	l = 0
	while l < k+1:
		print("*", end=" ")

		l += 1

	print()
	k += 1