"""
pattern_04

#-#-#-#-*
      * *
    * * *
  * * * *
* * * * *
"""

baris = int(input("Masukan jumlah baris: "))

for i in range(baris):
	for j in range((baris-i)-1):
		print(" ", end=" ")
	for k in range(i+1):
		print("*", end=" ")
	print()

print()

l = 0
while l < baris:
	m = 0
	while m < (baris-l)-1:
		print(" ", end=" ")

		m += 1

	n = 0
	while n < (l+1):
		print("*", end=" ")

		n += 1
	
	print()
	l += 1